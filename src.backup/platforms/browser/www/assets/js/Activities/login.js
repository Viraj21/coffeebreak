thisActivity = {
    requestLoginCertificate: function() {
        var input_data, isConnectedToServer;
        input_data = {
            email: document.getElementsByName('email')[0].value,
            password: document.getElementsByName('password')[0].value,
            action: 'account:certify-device'
        };
        isConnectedToServer = false;
        
        jQuery.post(window.app.cbapi_address, input_data, function(output_data) {
            var error;
            isConnectedToServer = true;

            if (output_data.success === false) {
                error = output_data.error;
                if (error === 10005) {
                    alert('Bruhhhh... you haven\'t signed up with that email yet!');
                }
                if (error === 10006) {
                    alert('Bruhhhh... your password doesn\'t work with your email, yo :(');
                }
                if (error === 10007) {
                    return alert('Oh lol... something went wrong, and we don\'t know what');
                }
            } else {
                window.app.addLocalData('user_email', input_data.email);
                window.app.addLocalData('user_certificate_token', output_data.token);
                window.app.addLocalData('account_status', 10004);
                alert('Your account has been verified!');
                window.location = 'download_resources.html';
            }
        });
    },

    test: function() {
        if ($deviceReady) {
            console.log('Notification:')
            console.log(navigation)
            console.log(navigation.notification)
            navigation.notification.alert("Don't worry, we got your back!", ()=>{}, 'Forgot your password?', "reset now!");
        }
    }
};

$(document).ready(function() {
    document.body.height = window.clientHeight;
    document.getElementsByClassName('container')[0].height = window.clientHeight;
    return $('.header-left').on('click', function() {
        return window.location = 'index.html';
    });
});