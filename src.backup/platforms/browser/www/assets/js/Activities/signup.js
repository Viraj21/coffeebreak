
window.thisActivity = {
    signup: function() {
        var input_data;
        input_data = {
            type: "jsonp",
            fname: document.getElementsByName('fname')[0].value,
            lname: document.getElementsByName('lname')[0].value,
            email: document.getElementsByName('email')[0].value,
            password: document.getElementsByName('password')[0].value,
            gender: document.getElementsByName('gender')[0].value,
            dob: document.getElementsByName('dob')[0].value,
            action: 'account:signup'
        };

        jQuery.post(window.app.cbapi_address, input_data, function(output_data) {
            if (output_data.success === false) {
                if (output_data.error_code === 10000) {
                    return alert('Sorry, but according to our database, your email is already in use!');
                }
            } else {
                alert('Great! Your account has been created!');
                window.location = 'index.html';
            }
        });
    }
};

$(document).ready(function() {
    return $('.header-left').on('click', function() {
        return window.location = 'index.html';
    });
});