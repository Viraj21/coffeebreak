config.convo = {
    topics: [
        "Politics",
        "TV Shows",
        "Movies",
        "Books",
        "Music",
        "Visual Art",
        "Technology",
        "Food"
    ]
};