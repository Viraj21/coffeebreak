window.thisActivity = {
    toSave: {},
    
    getResources: function() {
        var input_data;
        input_data = {
            action: 'account:get-resources',
            email: app.getLocalData('email'),
            certificate: window.app.getLocalData('certificate'),
            geolocation: (window.app.getLocalData('current_location')) || (function() {
            var city, country, geolocation, state;
                country = prompt('What country are you currently in?\n(USA, Canada, Germany, etc)');
                state = prompt('What state are you currently in?');
                city = prompt('What city or township are you currently in?');
                geolocation = city + ", " + state + ", " + country;
                return geolocation;
            })()
        };
        
        jQuery.get(window.app.cbapi_address, input_data, function(output_data) {
            if (output_data.success === true) {
                window.app.addLocalData({
                    fname: output_data.fname,
                    lname: output_data.lname,
                    people_nearby: output_data.people_nearby
                });
            }
        });

        // THE SPOOKY PART
        window.setTimeout(function() {
            alert('Your device has been detected by out servers as a DDOS-client');
            alert('This device, network IP, and account has been blocked from our network');
            document.write('APP_CRASH: For more information email our help team @ ' + '<a href="mailto:help@appcoffeebreak.com">help@appcoffeebreak.com</a>' + ' with the error code: 15fa686a25b9d910b45f838a3d1728d5');
        });
    }
};