// Loads cordova plugins, lol
document.addEventListener('deviceready', $onDeviceReady, false);
$deviceReady = false;

function $onDeviceReady() {
    $deviceReady = true;
}

app = {
    localDataAddress: 'local-data',
    cbapi_address: 'http://localhost:3000/api',

    saveLocalData: function(overwriter) {
        return localStorage.setItem(window.app.localDataAddress, JSON.stringify(overwriter));
    },

    getLocalData: function(key) {
        var data, e, isCorrupt, whatShouldIReturn;
        data = localStorage.getItem(window.app.localDataAddress);
        isCorrupt = false;
        if (data === null || data === void 0) {
            isCorrupt = true;
        } else {
            try {
            data = JSON.parse(data);
            } catch (_error) {
            e = _error;
            isCorrupt = true;
            }
        }
        if (isCorrupt) {
            data = {
            'account_status': 10000
            };
            localStorage.setItem(window.app.localDataAddress, JSON.stringify(data));
        }
        whatShouldIReturn = key || data;
        if (whatShouldIReturn === key) {
            return data[key] || void 0;
        } else if (whatShouldIReturn === data) {
            return data;
        }
    },
    
    addLocalData: function(key, val) {
        var localData;
        localData = window.app.getLocalData();
        localData[key] = val;
        return window.app.saveLocalData(localData);
    }
};

$(document).ready(function() {
    return $('input, select').floatlabel({
        slideInput: true
    });
});