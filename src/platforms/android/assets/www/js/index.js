var app = {
    // CORDOVA BUILTIN
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        //app.receivedEvent('deviceready');
        alert('Fired')
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },

    // // CORDOVA BUILTIN



    // APP SCRIPTS

    localDataAddress: 'local-data',
    cbapi_address: 'http://localhost:3000/api',

    saveLocalData: function(overwriter) {
        return localStorage.setItem(app.localDataAddress, JSON.stringify(overwriter));
    },

    getLocalData: function(key) {
        var data, e, isCorrupt, whatShouldIReturn;
        data = localStorage.getItem(app.localDataAddress);
        isCorrupt = false;
        if (data === null || data === void 0) {
            isCorrupt = true;
        } else {
            try {
                data = JSON.parse(data);
            } catch (_error) {
                e = _error;
                isCorrupt = true;
            }
        }
        if (isCorrupt) {
            data = {
                'account_status': 10000
            };
            localStorage.setItem(app.localDataAddress, JSON.stringify(data));
        }
        whatShouldIReturn = key || data;
        if (whatShouldIReturn === key) {
            return data[key] || void 0;
        } else if (whatShouldIReturn === data) {
            return data;
        }
    },
    
    addLocalData: function(key, val) {
        var localData;
        localData = app.getLocalData();
        localData[key] = val;
        return app.saveLocalData(localData);
    },

    test: function() {
        navigator.notification.alert("Don't worry, we got your back!", ()=>{}, 'Forgot your password?', "reset now!");
    }

    // // APP SCRIPTS
};

$(document).ready(function() {
    app.initialize();
})